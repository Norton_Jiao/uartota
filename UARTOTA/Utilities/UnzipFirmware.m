//
//  UnzipFirmware.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 07/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "UnzipFirmware.h"
#import "SSZipArchive.h"
#import "AccessFileSystem.h"

@implementation UnzipFirmware

- (NSArray *)unzipFirmwareFiles:(NSURL *)zipFileURL
{
    NSMutableArray *filesURL = [[NSMutableArray alloc] init];
    NSString *outputPath = [self cachesPath:@"/UnzipFiles"];
    [SSZipArchive unzipFileAtPath:[zipFileURL path] toDestination:outputPath delegate:self];
    AccessFileSystem *fileSystem = [[AccessFileSystem alloc]init];
    
    NSString *softdevicePath, *bootloaderPath, *applicationPath;
    NSArray *files = [fileSystem getAllFilesFromDirectory:outputPath];
    
    for (NSString* file in files) {
        if ([file isEqualToString:@"softdevice.hex"]) {
            softdevicePath = [outputPath stringByAppendingPathComponent:@"softdevice.hex"];
            [filesURL addObject:[NSURL fileURLWithPath:softdevicePath]];
        } else if ([file isEqualToString:@"bootloader.hex"]) {
            bootloaderPath = [outputPath stringByAppendingPathComponent:@"bootloader.hex"];
            [filesURL addObject:[NSURL fileURLWithPath:bootloaderPath]];
        } else if ([file isEqualToString:@"application.hex"]) {
            applicationPath = [outputPath stringByAppendingPathComponent:@"application.hex"];
            [filesURL addObject:[NSURL fileURLWithPath:applicationPath]];
        }
    }
    return [filesURL copy];
}

- (NSString *)cachesPath:(NSString *)directory {
	NSString *path = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject]
                      stringByAppendingPathComponent:@"com.nordicsemi.nRFToolbox"];
	if (directory)
		path = [path stringByAppendingPathComponent:directory];
    
	NSFileManager *fileManager = [NSFileManager defaultManager];    
	if (![fileManager fileExistsAtPath:path]) {
		[fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
	} else {
        [fileManager removeItemAtPath:path error:nil];
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
	return path;
}

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath
{
    NSLog(@"zipArchiveDidUnzipArchiveAtPath, path: %@, unzippedPath: %@",path,unzippedPath);
}

- (void)zipArchiveDidUnzipFileAtIndex:(NSInteger)fileIndex totalFiles:(NSInteger)totalFiles archivePath:(NSString *)archivePath fileInfo:(unz_file_info)fileInfo
{
    NSLog(@"zipArchiveDidUnzipFileAtIndex, fileIndex: %ld, totalFiles: %ld, archivePath: %@", (long)fileIndex, (long)totalFiles, archivePath);
}

- (void)zipArchiveProgressEvent:(NSInteger)loaded total:(NSInteger)total
{
    NSLog(@"zipArchiveProgressEvent, loaded: %ld, total: %ld", (long)loaded, (long)total);
}

- (void)zipArchiveWillUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo
{
    NSLog(@"zipArchiveWillUnzipArchiveAtPath, path: %@", path);
}

- (void)zipArchiveWillUnzipFileAtIndex:(NSInteger)fileIndex totalFiles:(NSInteger)totalFiles archivePath:(NSString *)archivePath fileInfo:(unz_file_info)fileInfo
{
    NSLog(@"zipArchiveWillUnzipFileAtIndex fileIndex: %ld totalFiles: %ld archivePath: %@", (long)fileIndex, (long)totalFiles, archivePath);
}



@end
