//
//  Constants.h
//  nRF Toolbox
//
//  Created by Aleksander Nowakowski on 13/12/13.
//  Copyright (c) 2013 Nordic Semiconductor. All rights reserved.
//

#ifndef nRF_Toolbox_Constants_h
#define nRF_Toolbox_Constants_h

// UART UUIDs
static NSString *const UART_SERVICE_UUID = @"6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
static NSString *const UART_TX_CHARACTERISTIC_UUID = @"6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
static NSString *const UART_RX_CHARACTERISTIC_UUID = @"6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
static NSString *const UART_RW_CHARACTERISTIC_UUID = @"6E400004-B5A3-F393-E0A9-E50E24DCCA9E";
static NSString *const UART_DEVICE_INFORMATION_UUID = @"180A";
static NSString *const UART_HARDWARE_REVERSION_UUID = @"2A27";

// BOOTLOADER UUID
static NSString *const BOOTLOADER_SERVICE_UUID = @"00001530-1212-EFDE-1523-785FEABCD123";
static NSString *const BOOTLOADER_CONTROL_CHARACTERISTIC_UUID = @"00001531-1212-EFDE-1523-785FEABCD123";
static NSString *const BOOTLOADER_PACKET_CHARACTERISTIC_UUID = @"00001532-1212-EFDE-1523-785FEABCD123";

// FIRMWARE TYPE
static NSString *const FIRMWARE_TYPE_APPLICATION = @"application";
static NSString *const FIRMWARE_TYPE_SOFTDEVICE = @"softdevice";
static NSString *const FIRMWARE_TYPE_BOOTLOADER = @"bootloader";
static NSString *const FIRMWARE_TYPE_BOTH_SOFTDEVICE_BOOTLOADER = @"softdevice and bootloader";

static NSString *const DFU_NUMBER_OF_PACKETS = @"dfu_number_of_packets";

static NSString *const DFU_FILE_TYPE = @"fileTypeForOTA";

#endif
