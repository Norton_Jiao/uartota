//
//  NSData+Conversion.m
//  UARTOTA
//
//  Created by Norton on 11/3/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import "NSData+Conversion.h"

@implementation NSData (Conversion)

- (NSString *)MACHexadecimalToMACString
{
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger dataLength = [self length];
    NSMutableString *hexString = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lX:", (unsigned long)dataBuffer[i]]];
    
    return [hexString substringToIndex:[hexString length] - 1];
}

- (NSInteger)integerHexadecimalToInteger
{
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer) {
        return 0;
    }
    return ((0x000000FF & ((NSInteger)dataBuffer[3])) << 24) |
    ((0x000000FF & ((NSInteger)dataBuffer[2])) << 16) |
    ((0x000000FF & ((NSInteger)dataBuffer[1])) << 8) |
    (0x000000FF & (NSInteger)dataBuffer[0]);
}

@end
