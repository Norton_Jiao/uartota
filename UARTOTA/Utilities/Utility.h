//
//  Utility.h
//  nRFDeviceFirmwareUpdate
//
//  Created by Nordic Semiconductor on 22/05/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface Utility : NSObject

struct DFUResponse
{
    uint8_t responseCode;
    uint8_t requestedCode;
    uint8_t responseStatus;
};

typedef NS_ENUM(Byte, FileExtension)
{
    HEX,
    ZIP,
    INI,
    BIN
};

typedef NS_ENUM(Byte, DFUEvents)
{
    START_DFU_REQUEST = 0X01,
    INITIALIZE_DFU_PARAMETERS_REQUEST = 0X02,
    RECEIVE_FIRMWARE_IMAGE_REQUEST = 0X03,
    VALIDATE_FIRMWARE_REQUEST = 0X04,
    ACTIVATE_AND_RESET_REQUEST = 0X05,
    RESET_SYSTEM = 0X06,
    PACKET_RECEIPT_NOTIFICATION_REQUEST = 0X08,
    RESPONSE_CODE = 0X10,
    PACKET_RECEIPT_NOTIFICATION_RESPONSE = 0X11
};

typedef NS_ENUM(Byte, DFUOperationStatus)
{
    OPERATION_SUCCESSFUL_RESPONSE = 0X01,
    OPERATION_INVALID_RESPONSE = 0X02,
    OPERATION_NOT_SUPPORTED_RESPONSE = 0X03,
    DATA_SIZE_EXCEEDS_LIMIT_RESPONSE = 0X04,
    CRC_ERROR_RESPONSE = 0X05,
    OPERATION_FAILED_RESPONSE = 0X06
};

typedef NS_ENUM(Byte, DFUFirmwareTypes)
{
    SOFTDEVICE = 0X01,
    BOOTLOADER = 0X02,
    SOFTDEVICE_AND_BOOTLOADER = 0X03,
    APPLICATION = 0X04
    
};

extern int PACKETS_NOTIFICATION_INTERVAL;
extern int const PACKET_SIZE;

+ (NSArray *)getFirmwareTypes;
+ (NSString *)stringFileExtension:(FileExtension)extension;
+ (NSString *)getDFUHelpText;
+ (NSString *)getEmptyUserFilesText;
+ (NSString *)getDFUAppFileHelpText;
+ (void)showAlert:(NSString *)message;

@end
