//
//  NSData+Conversion.h
//  UARTOTA
//
//  Created by Norton on 11/3/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Conversion)

// Convert the MAC address hexadecimal data to a MAC address string
- (NSString *)MACHexadecimalToMACString;
// Convert the integer hexadecimal data to integer
- (NSInteger)integerHexadecimalToInteger;

@end
