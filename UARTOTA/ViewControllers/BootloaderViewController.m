//
//  BootloaderViewController.m
//  UARTOTA
//
//  Created by Norton on 11/3/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import "BootloaderViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "AppFilesTableViewController.h"
#import "FileTypeTableViewController.h"

#import "Utility.h"
#import "Constants.h"
#import "AccessFileSystem.h"
#import "UnzipFirmware.h"
#import "DFUOperations.h"
#import "NSData+Conversion.h"

@interface BootloaderViewController()
<
CBCentralManagerDelegate,
FileSelectionDelegate,
DFUOperationsDelegate
>

@property (assign, nonatomic) DFUFirmwareTypes enumFirmwareType;
@property (strong, nonatomic) CBCentralManager *DFUManager;
@property (strong, nonatomic) CBPeripheral *DFUPeripheral;
@property (strong, nonatomic) DFUOperations *dfuOperations;
@property (strong, nonatomic) CBUUID *filterUUID;

@property (weak, nonatomic) IBOutlet UILabel *DFULabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (weak, nonatomic) IBOutlet UILabel *waittingLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *uploadProgress;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

@property (strong, nonatomic) NSURL *selectedFileURL;
@property (strong, nonatomic) NSURL *softdeviceURL;
@property (strong, nonatomic) NSURL *bootloaderURL;
@property (strong, nonatomic) NSURL *applicationURL;
@property (assign, nonatomic) NSUInteger selectedFileSize;
@property (strong, nonatomic) NSString *selectedFileType;

@property (assign, nonatomic) BOOL isSelectedFileZipped;
@property (assign, nonatomic) BOOL isConnected;
@property (assign, nonatomic) BOOL isTransferring;
@property (assign, nonatomic) BOOL isTransfered;
@property (assign, nonatomic) BOOL isTransferCancelled;
@property (assign, nonatomic) BOOL isErrorKnown;

@end

@implementation BootloaderViewController

#pragma mark - lifecycle

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        PACKETS_NOTIFICATION_INTERVAL = [[[NSUserDefaults standardUserDefaults] valueForKey:DFU_NUMBER_OF_PACKETS] intValue];
        self.dfuOperations = [[DFUOperations alloc] initWithDelegate:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    dispatch_queue_t centralQueue = dispatch_queue_create("no.nordicsemi.ios.nrftoolbox", DISPATCH_QUEUE_SERIAL);
    self.DFUManager = [[CBCentralManager alloc] initWithDelegate:self queue:centralQueue];

    UITapGestureRecognizer *selectFile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectOTAFile:)];
    [_nameLabel addGestureRecognizer:selectFile];
    UITapGestureRecognizer *selectType = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectFileType:)];
    [_typeLabel addGestureRecognizer:selectType];

    [self loadDefaultDFUFile];
    [self loadDefaultFileType];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)clearUI
{
    self.DFUPeripheral = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _DFULabel.text = @"DEFAULT DFU";
        _waittingLabel.text = @"waiting ...";
        [_waittingLabel setHidden:YES];
        _uploadProgress.progress = 0.0f;
        _uploadProgress.hidden = YES;
        _percentLabel.hidden = YES;
        _percentLabel.text = @"";
        [_uploadButton setTitle:@"UPLOAD" forState:UIControlStateNormal];
        [_uploadButton setEnabled:NO];
    });
}

- (void)enableUploadButton
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_selectedFileType && _selectedFileSize > 0) {
            if (![self isValidFileSelected]) {
                [Utility showAlert:[self getFileValidationMessage]];
                return;
            }
            
            if (_DFUPeripheral && _isConnected) {
                [_uploadButton setEnabled:YES];
            }
        }
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self scanForPeripherals:YES];
    }
}

- (void)scanForPeripherals:(BOOL)enable
{
    if (enable) {
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
        if (_filterUUID) {
            [_DFUManager scanForPeripheralsWithServices:@[ _filterUUID ] options:options];
        } else {
            [_DFUManager scanForPeripheralsWithServices:nil options:options];
        }
    } else {
        [_DFUManager stopScan];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if ([self matchedDFUDeviceWithUARTInfo:advertisementData]) {
        [self scanForPeripherals:NO];
        [self connectMatchedPeripheral:peripheral];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [indicator setCenter:self.view.center];
            [self.view addSubview:indicator];
            [self.view setUserInteractionEnabled:NO];
            [self.navigationItem setHidesBackButton:YES animated:YES];
            [indicator startAnimating];
            
            [NSTimer scheduledTimerWithTimeInterval:6.0
                                             target:self
                                           selector:@selector(resumeApplicationToBootloader:)
                                           userInfo:nil
                                            repeats:NO];
        });
    }
}

- (BOOL)matchedDFUDeviceWithUARTInfo:(NSDictionary *)data
{
    NSData *manuData = [data objectForKey:CBAdvertisementDataManufacturerDataKey];
    if (!manuData || [manuData length] != 6) {
        return NO;
    }

    Byte *bytes = (Byte *)[manuData  bytes];
    Byte MacBytes[] = {bytes[5], bytes[4], bytes[3], bytes[2], bytes[1], bytes[0]};
    NSString *peripheralMac = [[NSData dataWithBytes:MacBytes length:6] MACHexadecimalToMACString];

    if ([_uniqueID isEqualToString:peripheralMac]) {
        return YES;
    }
    return NO;
}

- (void)connectMatchedPeripheral:(CBPeripheral *)peripheral
{
    [_dfuOperations setCentralManager:_DFUManager];
    _DFULabel.text = peripheral.name;
    [_dfuOperations connectDevice:peripheral];
}

#pragma mark - DFUOperationsDelegate

- (void)onDeviceConnected:(CBPeripheral *)peripheral
{
    _DFUPeripheral = peripheral;
    self.isConnected = YES;
    [self enableUploadButton];
}

- (void)onDeviceDisconnected:(CBPeripheral *)peripheral
{
    self.isConnected = NO;
    self.isTransferring = NO;

    dispatch_async(dispatch_get_main_queue(), ^{
        [self clearUI];
        
        if (!_isTransfered && !_isTransferCancelled && !_isErrorKnown) {
            [self scanForPeripherals:YES];
        }
        
        self.isTransferCancelled = NO;
        self.isTransfered = NO;
        self.isErrorKnown = NO;
    });
}
- (void)onDFUStarted
{
    self.isTransferring = YES;

    dispatch_async(dispatch_get_main_queue(), ^{
        [_uploadButton setEnabled:YES];
        [_uploadButton setTitle:@"Cancel" forState:UIControlStateNormal];
        _waittingLabel.text = [self getUploadStatusMessage];
    });
}
- (void)onDFUCancelled
{
    self.isTransferring = NO;
    self.isTransferCancelled = YES;
}

- (void)onSoftDeviceUploadStarted
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _waittingLabel.text = [self getUploadStatusMessage];
    });
}

- (void)onSoftDeviceUploadCompleted
{
    self.isTransferring = NO;
    self.isTransfered = YES;
    
    [self clearUI];
}

- (void)onBootloaderUploadStarted
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _waittingLabel.text = [self getUploadStatusMessage];
    });
}

- (void)onBootloaderUploadCompleted
{
    self.isTransferring = NO;
    self.isTransfered = YES;
    
    [self clearUI];
}

- (void)onTransferPercentage:(int)percentage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _percentLabel.text = [NSString stringWithFormat:@"%d %%", percentage];
        [_uploadProgress setProgress:((float)percentage / 100.0) animated:YES];
    });
}

- (void)onSuccessfulFileTranferred
{
    self.isTransferring = NO;
    self.isTransfered = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utility showAlert:[NSString stringWithFormat:@"%lu bytes transfered in %lu seconds", (unsigned long)_dfuOperations.binFileSize, (unsigned long)_dfuOperations.uploadTimeInSeconds]];
    });
}

- (void)resumeApplicationToBootloader:(NSTimer *)timer
{
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIActivityIndicatorView class]]) {
            UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)subView;
            [indicator stopAnimating];
            [indicator removeFromSuperview];
        }
    }
    
    [self.view setUserInteractionEnabled:YES];
    
    [self.navigationItem setHidesBackButton:NO animated:YES];
}

- (void)onError:(NSString *)errorMessage
{
    self.isErrorKnown = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utility showAlert:errorMessage];
        [self clearUI];
    });
}

- (NSString *)getUploadStatusMessage
{
    switch (_enumFirmwareType) {
        case APPLICATION:
            return @"Uploading application ...";
        case SOFTDEVICE:
        case SOFTDEVICE_AND_BOOTLOADER:
            return @"Uploading softdevice ...";
        case BOOTLOADER:
            return @"Uploading bootloader ...";
            
        default:
            return @"Waitting ...";
    }
}

#pragma mark - FileSelectionDelegate

- (void)onFileSelected:(NSURL *)fileURL
{
    _selectedFileURL = fileURL;

    [self setSelectedDFUFileWithFileURL:fileURL];
}

#pragma mark - Actions

- (IBAction)uploadOTAFile:(UIButton *)sender {
    if (_isTransferring) {
        [_dfuOperations cancelDFU];
    } else {
        [self performDFUActions];
    }
}

- (void)performDFUActions
{
    [_waittingLabel setHidden:NO];
    [_uploadProgress setHidden:NO];
    [_percentLabel setHidden:NO];
    [_uploadButton setEnabled:NO];
    
    if (_isSelectedFileZipped) {
        switch (_enumFirmwareType) {
            case APPLICATION:
                [_dfuOperations performDFUOnFile:_applicationURL firmwareType:_enumFirmwareType];
                break;
            case SOFTDEVICE:
                [_dfuOperations performDFUOnFile:_softdeviceURL firmwareType:_enumFirmwareType];
                break;
            case BOOTLOADER:
                [_dfuOperations performDFUOnFile:_bootloaderURL firmwareType:_enumFirmwareType];
                break;
            case SOFTDEVICE_AND_BOOTLOADER:
                [_dfuOperations performDFUOnFiles:_softdeviceURL bootloaderURL:_bootloaderURL firmwareType:_enumFirmwareType];
                break;
                
            default:
                break;
        }
    } else {
        [_dfuOperations performDFUOnFile:_selectedFileURL firmwareType:_enumFirmwareType];
    }
    
}

- (void)selectOTAFile:(id)sender
{
    UITabBarController *fileTabs = [self.storyboard instantiateViewControllerWithIdentifier:@"FileListTabBarController"];
    
    UINavigationController *navController = [fileTabs.viewControllers firstObject];
    AppFilesTableViewController *appFilesVC = (AppFilesTableViewController *)navController.topViewController;
    appFilesVC.fileDelegate = self;
    
    [self.navigationController pushViewController:fileTabs animated:YES];
}

- (void)selectFileType:(id)sender
{
    FileTypeTableViewController *typeView = [self.storyboard instantiateViewControllerWithIdentifier:@"FileTypeTableViewController"];
    [self.navigationController pushViewController:typeView animated:YES];
}

- (IBAction)unwindFileTypeSelector:(UIStoryboardSegue *)sender
{
    FileTypeTableViewController *typeController = [sender sourceViewController];
    [self setFirmwareType:typeController.chosenFirmwareType];
}

- (void)setFirmwareType:(NSString *)firmwareType
{
    self.selectedFileType = firmwareType;
    _typeLabel.text = firmwareType;

    if ([firmwareType isEqualToString:FIRMWARE_TYPE_APPLICATION]) {
        _enumFirmwareType = APPLICATION;
    } else if ([firmwareType isEqualToString:FIRMWARE_TYPE_SOFTDEVICE]) {
        _enumFirmwareType = SOFTDEVICE;
    } else if ([firmwareType isEqualToString:FIRMWARE_TYPE_BOOTLOADER]) {
        _enumFirmwareType = BOOTLOADER;
    } else if ([firmwareType isEqualToString:FIRMWARE_TYPE_BOTH_SOFTDEVICE_BOOTLOADER]) {
        _enumFirmwareType = SOFTDEVICE_AND_BOOTLOADER;
    }

    [self enableUploadButton];
}

#pragma mark - Load default content

- (void)loadDefaultDFUFile
{
    AccessFileSystem *bootSystem = [[AccessFileSystem alloc] init];
    NSString *defaultPath = [bootSystem getAppDirectoryPath:@"firmwares"];
    
    NSArray *files;
    // Defaultly, load binary file for OTA
    NSString *fileType = [[NSUserDefaults standardUserDefaults] objectForKey:DFU_FILE_TYPE];
    if ([fileType isEqualToString:@"BIN"])
        files = [bootSystem getBinaryFilesFromDirectory:defaultPath];
    // else, load hex file or zip file for OTA
    else
        files = [bootSystem getRequiredFilesFromDirectory:defaultPath];

    if (files && files.count > 0)
        defaultPath = [defaultPath stringByAppendingPathComponent:[files lastObject]];
    else
        return;
    
    self.selectedFileURL = [NSURL fileURLWithPath:defaultPath];
    [self setSelectedDFUFileWithFileURL:_selectedFileURL];
}

- (void)setSelectedDFUFileWithFileURL:(NSURL *)fileURL
{
    if (fileURL) {
        NSString *fileName = [fileURL.path lastPathComponent];
        NSData *fileData = [NSData dataWithContentsOfURL:fileURL];
        self.selectedFileSize = fileData.length;

        // Get last three characters for file extension
        NSArray *nameComponents = [fileName componentsSeparatedByString:@"."];
        NSString *extension = [nameComponents lastObject];

        if ([extension isEqualToString:@"zip"]) {
            self.isSelectedFileZipped = YES;
            [self unzipFileWithFileURL:_selectedFileURL];
        } else {
            self.isSelectedFileZipped = NO;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            _nameLabel.text = fileName;
            _sizeLabel.text = [NSString stringWithFormat:@"%lu bytes", (unsigned long)_selectedFileSize];
        });
        [self enableUploadButton];
    } else {
        [Utility showAlert:[NSString stringWithFormat:@"File %@ not exist!", fileURL]];
    }
}

- (BOOL)isValidFileSelected
{
    if (_isSelectedFileZipped) {
        switch (_enumFirmwareType) {
            case SOFTDEVICE_AND_BOOTLOADER:
                if (_softdeviceURL && _bootloaderURL)
                    return YES;
            case SOFTDEVICE:
                if (_softdeviceURL)
                    return YES;
            case BOOTLOADER:
                if (_bootloaderURL)
                    return YES;
            case APPLICATION:
                if (_applicationURL)
                    return YES;
            default:
                return NO;
        }
        return NO;
    } else if(_enumFirmwareType == SOFTDEVICE_AND_BOOTLOADER) {
        return NO;
    }
    
    return YES;
}

- (NSString *)getFileValidationMessage
{
    NSString *message = [NSString stringWithFormat:@"not exist inside selected file %@", [_selectedFileURL lastPathComponent]];
    switch (_enumFirmwareType) {
        case APPLICATION:
            return [NSString stringWithFormat:@"application.hex %@", message];
        case SOFTDEVICE:
            return [NSString stringWithFormat:@"bootloader.hex %@", message];
        case BOOTLOADER:
            return [NSString stringWithFormat:@"softdevice.hex %@", message];
        case SOFTDEVICE_AND_BOOTLOADER:
            return @"For selected File Type, zip file is required having inside softdevice.hex and bootloader.hex";
            
        default:
            return @"Not valid File type";
    }
}

- (void)unzipFileWithFileURL:(NSURL *)fileURL
{
    self.softdeviceURL = nil;
    self.bootloaderURL = nil;
    self.applicationURL = nil;

    UnzipFirmware *unzipFiles = [[UnzipFirmware alloc] init];
    NSArray *filesURLs = [unzipFiles unzipFirmwareFiles:fileURL];
    for (NSURL *firmwareURL in filesURLs) {
        NSString *fileName = [[firmwareURL path] lastPathComponent];
        if ([fileName isEqualToString:@"softdevice.hex"]) {
            self.softdeviceURL = firmwareURL;
        } else if ([fileName isEqualToString:@"bootloader.hex"]) {
            self.bootloaderURL = firmwareURL;
        } else if ([fileName isEqualToString:@"application.hex"]) {
            self.applicationURL = firmwareURL;
        }
    }
}

- (void)loadDefaultFileType
{
    NSArray *fileTypes = [Utility getFirmwareTypes];

    [self setFirmwareType:[fileTypes firstObject]];
}

@end
