//
//  UARTViewController.m
//  UARTOTA
//
//  Created by Norton on 11/2/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import "UARTViewController.h"
#import "BootloaderViewController.h"

#import "UARTPeripheral.h"
#import "NSData+Conversion.h"
#import "Utility.h"

typedef NS_ENUM(Byte, ConnectionState)
{
    STATE_IDLE = 0X00,
    STATE_SCANNING = 0X01,
    STATE_CONNECTED = 0X02
};

static NSString *const  cellIdentifier = @"scannedUARTCell";

@interface UARTViewController () <CBCentralManagerDelegate, UARTPeripheralDelegate>

@property (weak, nonatomic) IBOutlet UILabel *deviceLabel;
@property (weak, nonatomic) IBOutlet UIButton *uartButton;
@property (weak, nonatomic) IBOutlet UITableView *scanTable;
@property (weak, nonatomic) IBOutlet UISegmentedControl *fileTypeSeg;

@property (strong, nonatomic) CBCentralManager *UARTManager;
@property (nonatomic) ConnectionState state;
@property (strong, nonatomic) UARTPeripheral *connectedPeripheral;
@property (strong, nonatomic) NSString *uniqueID;

@property (strong, nonatomic) NSMutableArray *scanedList;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation UARTViewController

#pragma mark - lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.UARTManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    self.scanedList = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_uartButton setEnabled:NO];
    if (_UARTManager.state == CBCentralManagerStatePoweredOn ) {
        [_UARTManager scanForPeripheralsWithServices:@[UARTPeripheral.uartServiceUUID]
                                             options:@{CBCentralManagerScanOptionAllowDuplicatesKey: [NSNumber numberWithBool:NO]}];
    }
    
    NSString *fileType = [[NSUserDefaults standardUserDefaults] objectForKey:DFU_FILE_TYPE];
    if ([fileType isEqualToString:@"BIN"]) {
        _fileTypeSeg.selectedSegmentIndex = 0;
    } else {
        _fileTypeSeg.selectedSegmentIndex = 1;
    }
    
    NSLog(@"------->>>>>>%@", [[NSUserDefaults standardUserDefaults] objectForKey:DFU_FILE_TYPE]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"bootloader"]) {
        BootloaderViewController *bootController = segue.destinationViewController;
        bootController.uniqueID = _uniqueID;
    }
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBCentralManagerStatePoweredOn) {
        [_UARTManager scanForPeripheralsWithServices:@[UARTPeripheral.uartServiceUUID]
                                             options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @NO}];
    } else {
        [Utility showAlert:@"Central Manager State is invalid, cannot scan for BLE device"];
    }
    
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSData *manuData = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    if (!manuData) return;
    
    Byte *bytes = (Byte *)[[manuData subdataWithRange:NSMakeRange(3, 6)] bytes];
    Byte MacBytes[] = {bytes[5] ^ 0X01, bytes[4], bytes[3], bytes[2], bytes[1], bytes[0]};
    NSString *peripheralMac = [[NSData dataWithBytes:MacBytes length:6] MACHexadecimalToMACString];

    BOOL added = NO;
    UARTPeripheral *newPeripheral = [[UARTPeripheral alloc] initWithPeripheral:peripheral
                                                                  uartDelegate:self
                                                                          RSSI:RSSI.intValue];
    newPeripheral.MACAddress = peripheralMac;
    
    for (UARTPeripheral *scannedPeripheral in _scanedList) {
        if ([scannedPeripheral.MACAddress isEqualToString:newPeripheral.MACAddress]) {
            added = YES;
            break;
        }
    }
    
    if (!added)
        [_scanedList addObject:newPeripheral];

    [_scanTable reloadData];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    if ([_connectedPeripheral.peripheral isEqual:peripheral]) {
        [NSTimer scheduledTimerWithTimeInterval:2.0
                                         target:self
                                       selector:@selector(enableViewUserInteraction:)
                                       userInfo:nil
                                        repeats:NO];
    }
}

- (void)enableViewUserInteraction:(NSTimer *)timer
{
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIActivityIndicatorView class]]) {
            UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)subView;
            [indicator stopAnimating];
            [indicator removeFromSuperview];
        }
    }

    [self.view setUserInteractionEnabled:YES];
    [_uartButton setEnabled:YES];
    UITableViewCell *cell = (UITableViewCell *)[_scanTable cellForRowAtIndexPath:_selectedIndexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [_connectedPeripheral didConnectPeripheral];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if ([_connectedPeripheral.peripheral isEqual:peripheral]) {
        [_uartButton setEnabled:NO];
        [self cleanConnection];
        [_connectedPeripheral didDisconnectPeripheral];
    }
}

- (void)cleanConnection
{
    _connectedPeripheral.peripheral = nil;
    _connectedPeripheral = nil;
    [_uartButton setEnabled:NO];
    [_scanedList removeAllObjects];
    [_scanTable reloadData];
}

#pragma mark - Actions

- (IBAction)switchToBootLoader:(UIButton *)sender
{
    Byte bytes[] = {0X51, 0X01, 0X01, 0X01, 0X01};
    NSData *data = [NSData dataWithBytes:bytes length:5];
    [_connectedPeripheral writeRawData:data];
}

- (IBAction)changeDefaultFileType:(UISegmentedControl *)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:DFU_FILE_TYPE];

    [defaults setObject:@"BIN" forKey:DFU_FILE_TYPE];
    if (sender.selectedSegmentIndex != 0) {
        [defaults setObject:@"HEX" forKey:DFU_FILE_TYPE];
    }
    [defaults synchronize];
}

#pragma mark - UARTPeripheralDelegate

- (void)didReceiveData:(NSData *)data
{
    Byte *bytes = (Byte *)data.bytes;
    
    if (bytes[0] == 0X55 && [self isValidResponseCommand:data]) {
        [self cleanConnection];
        [self performSegueWithIdentifier:@"bootloader" sender:nil];
    }
}

- (void)didReadHardwareRevisionString:(NSString *)string
{
    // ----------------
}

- (BOOL)isValidResponseCommand:(NSData *)command
{
    if (command.length == 7) {
        Byte *bytes = (Byte *)command.bytes;
        Byte checksum = bytes[3] ^ bytes[4] ^ bytes[5];
        if (checksum == bytes[6]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _scanedList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UARTPeripheral *peripheral = _scanedList[indexPath.row];
    cell.textLabel.text = peripheral.peripheral.name;
    
    // Update RSSI indicator
    int RSSI = peripheral.RSSI;
    UIImage* image;
    if (RSSI < -90) {
        image = [UIImage imageNamed: @"Signal_0"];
    } else if (RSSI < -70) {
        image = [UIImage imageNamed: @"Signal_1"];
    } else if (RSSI < -50) {
        image = [UIImage imageNamed: @"Signal_2"];
    } else {
        image = [UIImage imageNamed: @"Signal_3"];
    }
    cell.imageView.image = image;
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_UARTManager stopScan];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator setCenter:_scanTable.center];
    [self.view addSubview:indicator];
    [self.view setUserInteractionEnabled:NO];
    [indicator startAnimating];

    UARTPeripheral *peripheral = _scanedList[indexPath.row];
    self.connectedPeripheral = peripheral;
    [_UARTManager connectPeripheral:peripheral.peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
    self.selectedIndexPath = indexPath;
    self.uniqueID = peripheral.MACAddress;

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
