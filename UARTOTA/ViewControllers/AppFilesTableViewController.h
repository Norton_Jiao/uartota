//
//  AppFilesTableViewController.h
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 21/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FileSelectionDelegate <NSObject>

@required
- (void)onFileSelected:(NSURL *)fileURL;

@end

@interface AppFilesTableViewController : UITableViewController <UITabBarControllerDelegate>

//define delegate property
@property (weak, nonatomic) id<FileSelectionDelegate> fileDelegate;

@end
