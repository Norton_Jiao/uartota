//
//  UserFilesTableViewController.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 21/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "UserFilesTableViewController.h"
#import "AccessFileSystem.h"
#import "Utility.h"
#import "FolderFilesTableViewController.h"


@interface UserFilesTableViewController ()

@property (nonatomic, strong) NSMutableArray *files;
@property (nonatomic, strong) NSString *documentsDirectoryPath;
@property (nonatomic, strong) AccessFileSystem *fileSystem;

@property (weak, nonatomic) IBOutlet UITextView *emptyMessageView;

@end

@implementation UserFilesTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Background4"]]];
    self.fileSystem = [[AccessFileSystem alloc] init];
    self.documentsDirectoryPath = [self.fileSystem getDocumentsDirectoryPath];
    // self.files = [[self.fileSystem getDirectoriesAndRequiredFilesFromDocumentsDirectory] mutableCopy];
    
    // Defaultly, load the binary files for
    NSString *fileType = [[NSUserDefaults standardUserDefaults] objectForKey:DFU_FILE_TYPE];
    if ([fileType isEqualToString:@"BIN"])
        self.files = [[self.fileSystem getBinaryFilesFromDirectory:[self.fileSystem getDocumentsDirectoryPath]] mutableCopy];
    // else, load hex file or zip file for OTA
    else
        self.files = [[self.fileSystem getDirectoriesAndRequiredFilesFromDocumentsDirectory] mutableCopy];
    
    
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be re:created.
}

- (void)setEmptyTableMessage
{
    _emptyMessageView.hidden = NO;
    _emptyMessageView.editable = YES;
    [_emptyMessageView setFont:[UIFont systemFontOfSize:18.0]];
    _emptyMessageView.text = [Utility getEmptyUserFilesText];
    _emptyMessageView.editable = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionred
{
    if (self.files.count == 0) {
        [self setEmptyTableMessage];
        self.navigationItem.rightBarButtonItem.enabled = NO;
    } else {
        _emptyMessageView.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    return self.files.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserFilesCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *fileName = [self.files objectAtIndex:indexPath.row];
    NSString *filePath = [self.documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    UIImage *image = [UIImage imageNamed:@"file"];
    if ([self.fileSystem isDirectory:filePath]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ([fileName isEqualToString:@"Inbox"]) {
            image = [UIImage imageNamed:@"emailFolder"];
        } else {
            image = [UIImage imageNamed:@"folder"];
        }
    } else if ([self.fileSystem isGivenFile:fileName withExtension:ZIP]) {
        image = [UIImage imageNamed:@"zipFile"];
    }
    
    cell.imageView.image = image;
    cell.textLabel.text = [self.files objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *fileName = [self.files objectAtIndex:indexPath.row];
    NSString *filePath = [self.documentsDirectoryPath stringByAppendingPathComponent:fileName];
    
    if (![self.fileSystem isDirectory:filePath]) {
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        [self.fileDelegate onFileSelected:fileURL];
        
        UITabBarController *fileTabs = (UITabBarController *)self.parentViewController.parentViewController;
        [fileTabs.navigationController popViewControllerAnimated:YES];
    }    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *fileName = [self.files objectAtIndex:indexPath.row];
        if (![fileName isEqualToString:@"Inbox"]) {
            [self.files removeObjectAtIndex:indexPath.row];
            NSString *filePath = [self.documentsDirectoryPath stringByAppendingPathComponent:fileName];
            [self.fileSystem deleteFile:filePath];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else {
            [Utility showAlert:@"User can't delete Inbox directory"];
            [tableView reloadData];
        }
        
    }
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSIndexPath *selectionIndexPath = [self.tableView indexPathForSelectedRow];
    NSString *fileName = [self.files objectAtIndex:selectionIndexPath.row];
    NSString *filePath = [self.documentsDirectoryPath stringByAppendingPathComponent:fileName];
    if ([self.fileSystem isDirectory:filePath]) {
        return YES;
    }
    return NO;
}
 
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectionIndexPath = [self.tableView indexPathForSelectedRow];
    NSString *fileName = [self.files objectAtIndex:selectionIndexPath.row];
    NSString *filePath = [self.documentsDirectoryPath stringByAppendingPathComponent:fileName];
    if ([self.fileSystem isDirectory:filePath]) {
        FolderFilesTableViewController *folderVC = [segue destinationViewController];
        folderVC.directoryPath = filePath;
        folderVC.files = [[self.fileSystem getRequiredFilesFromDirectory:filePath] mutableCopy];
        folderVC.fileDelegate = self.fileDelegate;
    }
}

- (IBAction)cancelFileSelection:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
