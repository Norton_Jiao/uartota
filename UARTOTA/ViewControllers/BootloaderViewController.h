//
//  BootloaderViewController.h
//  UARTOTA
//
//  Created by Norton on 11/3/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BootloaderViewController : UIViewController

@property (strong, nonatomic) NSString *uniqueID;

@end
