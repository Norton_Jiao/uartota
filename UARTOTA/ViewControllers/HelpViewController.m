//
//  HelpViewController.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 06/02/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "HelpViewController.h"
#import "Utility.h"

@interface HelpViewController ()

@property (weak, nonatomic) IBOutlet UITextView *helpTextView;

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _helpTextView.text = [Utility getDFUHelpText];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
