//
//  AppFilesTableViewController.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 21/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "AppFilesTableViewController.h"
#import "AccessFileSystem.h"
#import "UserFilesTableViewController.h"

@interface AppFilesTableViewController ()

@property (nonatomic, strong) NSArray *files;
@property (nonatomic, strong) NSString *appDirectoryPath;
@property (nonatomic, strong) AccessFileSystem *fileSystem;

@end

@implementation AppFilesTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tabBarController.delegate = self;
    self.fileSystem = [[AccessFileSystem alloc] init];
    self.appDirectoryPath = [_fileSystem getAppDirectoryPath:@"firmwares"];
    self.files = [_fileSystem getAllFilesFromAppDirectory:@"firmwares"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TabBarController delegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    UINavigationController *navController = [tabBarController.viewControllers objectAtIndex:1];
    UserFilesTableViewController *userFilesVC = (UserFilesTableViewController *)[navController topViewController];
    userFilesVC.fileDelegate = self.fileDelegate;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.files.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppFilesCell" forIndexPath:indexPath];
    NSString *fileName = [self.files objectAtIndex:indexPath.row];
    
    // Configure the cell...
    cell.textLabel.text = [self.files objectAtIndex:indexPath.row];

    UIImage *image = [UIImage imageNamed:@"file"];
    if ([self.fileSystem isGivenFile:fileName withExtension:ZIP])
        image = [UIImage imageNamed:@"zipFile"];

    cell.imageView.image = image;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *fileName = [self.files objectAtIndex:indexPath.row];
    NSString *filePath = [self.appDirectoryPath stringByAppendingPathComponent:fileName];
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    
    [self.fileDelegate onFileSelected:fileURL];
    UITabBarController *fileTabs = (UITabBarController *)self.parentViewController.parentViewController;
    [fileTabs.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelFileSelection:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    // [self dismissViewControllerAnimated:YES completion:nil];
}


@end
