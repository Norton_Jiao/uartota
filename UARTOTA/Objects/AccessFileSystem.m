//
//  AccessFileSystem.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 09/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "AccessFileSystem.h"
#import "Utility.h"

@implementation AccessFileSystem

- (NSString *)getAppDirectoryPath:(NSString *)directory
{
    NSString *appPath = [[NSBundle mainBundle] resourcePath];
    return [appPath stringByAppendingPathComponent:directory];
}

- (NSString *)getDocumentsDirectoryPath
{
    return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

- (NSArray *)getAllFilesFromAppDirectory:(NSString *)directory
{
    NSString *firmwaresPath = [self getAppDirectoryPath:directory];
    return [self getAllFilesFromDirectory:firmwaresPath];
}

- (NSArray *)getDirectoriesAndRequiredFilesFromDocumentsDirectory
{
    NSMutableArray *allFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"Error in opening directory path: %@", [self getDocumentsDirectoryPath]]];
        return nil;
    } else {
        [allFileNames addObjectsFromArray:[self getSubDirectoriesInDocumentsDirectory]];
        [allFileNames addObjectsFromArray:[self getRequiredFilesFromDocumentsDirectory]];
        return [allFileNames copy];
    }
}

- (NSArray *)getRequiredFilesFromDocumentsDirectory
{
    return [self getRequiredFilesFromDirectory:[self getDocumentsDirectoryPath]];
}

- (NSArray *)getAllFilesFromDocumentsDirectory
{
    return [self getAllFilesFromDirectory:[self getDocumentsDirectoryPath]];
}

- (NSArray *)getRequiredFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *requiredFilesNames = [[NSMutableArray alloc] init];
    [requiredFilesNames addObjectsFromArray:[self getHexFilesFromDirectory:directory]];
    [requiredFilesNames addObjectsFromArray:[self getZipFilesFromDirectory:directory]];
    return requiredFilesNames;
}


- (NSArray *)getCRCFilesFromDocumentsDirectory
{
    NSMutableArray *binaryFiles = [[NSMutableArray alloc] init];
    [binaryFiles addObjectsFromArray:[self getCRCFilesFromDirectory:[self getDocumentsDirectoryPath]]];
    return binaryFiles;
}

- (NSArray *)getHexFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *hexFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"HEX--Error in opening directory path: %@", directory]];
        return nil;
    } else {
        for (NSString *fileName in fileNames) {
            if ([self isGivenFile:fileName withExtension:HEX])
                [hexFileNames addObject:fileName];
        }
        return [hexFileNames copy];
    }
}

- (NSArray *)getZipFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *zipFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"ZIP--Error in opening directory path: %@", directory]];
        return nil;
    } else {
        for (NSString *fileName in fileNames) {
            if ([self isGivenFile:fileName withExtension:ZIP])
                [zipFileNames addObject:fileName];
        }
        return [zipFileNames copy];
    }
}

- (NSArray *)getCRCFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *crcFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"CRC--Error in opening directory path: %@", directory]];
        return nil;
    } else {
        for (NSString *fileName in fileNames) {
            if ([self isGivenFile:fileName withExtension:INI])
                [crcFileNames addObject:[[self getDocumentsDirectoryPath] stringByAppendingPathComponent:fileName]];
        }
        return [crcFileNames copy];
    }
}

- (NSArray *)getBinaryFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *binFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"BIN---Error in opening directory path: %@", directory]];
        return nil;
    } else {
        for (NSString *fileName in fileNames) {
            if ([self isGivenFile:fileName withExtension:BIN])
                [binFileNames addObject:fileName];
        }
        return [binFileNames copy];
    }
}

- (NSArray *)getAllFilesFromDirectory:(NSString *)directory
{
    NSMutableArray *allFileNames = [[NSMutableArray alloc] init];
    NSError *error;
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"Error in opening directory path: %@", directory]];
        return nil;
    } else {
        for (NSString *fileName in fileNames) {
            [allFileNames addObject:fileName];
        }
        return [allFileNames copy];
    }
}

- (NSArray *)getFilesFromDirectory:(NSString *)directory withExtension:(FileExtension)Extension
{
    NSMutableArray *filesWithExt = [[NSMutableArray alloc] init];
    NSArray *fileNames = [self getAllFilesFromDirectory:directory];
    for (NSString *fileName in fileNames) {
        if ([self isGivenFile:fileName withExtension:Extension])
            [filesWithExt addObject:fileName];
    }
    return [filesWithExt copy];
}

- (NSArray *)getSubDirectoriesInDocumentsDirectory
{
    NSMutableArray *directories = [[NSMutableArray alloc] init];
    NSArray *documentsFiles = [self getAllFilesFromDocumentsDirectory];
    NSString *documentsDirectoryPath = [self getDocumentsDirectoryPath];
    NSString *filePath;
    for (NSString *file in documentsFiles) {
        filePath = [documentsDirectoryPath stringByAppendingPathComponent:file];
        if ([self isDirectory:filePath]) {
            [directories addObject:file];
            
        }
    }
    return [directories copy];
}

- (BOOL)isDirectory:(NSString *)path
{
    BOOL isDirectory;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory]) {
        return isDirectory;
    }
    return NO;
}

- (BOOL)isGivenFile:(NSString *)fileName withExtension:(FileExtension)extension
{
    NSString *fileExtension = [[fileName pathExtension] uppercaseString];
    if ([fileExtension isEqualToString:[Utility stringFileExtension:extension]])
        return YES;
    return NO;
}

- (void)deleteFile:(NSString *)path
{
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"%@ cannot be removed", path]];
    }
}

@end
