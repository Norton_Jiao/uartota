//
//  UARTPeripheral.h
//  UARTOTA
//
//  Created by Norton on 11/2/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol UARTPeripheralDelegate <NSObject>

@required
- (void)didReceiveData:(NSData *)data;

@optional
- (void)didReadHardwareRevisionString:(NSString *)string;

@end

@interface UARTPeripheral : NSObject

@property (strong, nonatomic) CBPeripheral *peripheral;
@property id<UARTPeripheralDelegate> uartDelegate;
@property (assign, nonatomic) int RSSI;
@property (strong, nonatomic) NSString *MACAddress;

+ (CBUUID *)uartServiceUUID;

- (UARTPeripheral *)initWithPeripheral:(CBPeripheral *)peripheral uartDelegate:(id<UARTPeripheralDelegate>)uartDelegate RSSI:(int)RSSI;
- (void)writeString:(NSString *)string;
- (void)writeRawData:(NSData *)data;
- (void)didConnectPeripheral;
- (void)didDisconnectPeripheral;

@end
