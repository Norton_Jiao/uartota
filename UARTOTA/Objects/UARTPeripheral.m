//
//  UARTPeripheral.m
//  UARTOTA
//
//  Created by Norton on 11/2/14.
//  Copyright (c) 2014 Mophie. All rights reserved.
//

#import "UARTPeripheral.h"
#import "Constants.h"
#import "Utility.h"

@interface UARTPeripheral() <CBPeripheralDelegate>

@property (strong, nonatomic) CBService *uartService;
@property (strong, nonatomic) CBCharacteristic *rxCharacteristic;
@property (strong, nonatomic) CBCharacteristic *txCharacteristic;
@property (strong, nonatomic) CBCharacteristic *rwCharacteristic;

@end

@implementation UARTPeripheral

#pragma mark - UUIDs

+ (CBUUID *)uartServiceUUID
{
    // return [CBUUID UUIDWithString:@"6e400001-b5a3-f393-e0a9-e50e24dcca9e"];
    return [CBUUID UUIDWithString:UART_SERVICE_UUID];
}

- (CBUUID *)txCharacteristicUUID
{
    // return [CBUUID UUIDWithString:@"6e400002-b5a3-f393-e0a9-e50e24dcca9e"];
    return [CBUUID UUIDWithString:UART_TX_CHARACTERISTIC_UUID];
}

- (CBUUID *)rxCharacteristicUUID
{
    // return [CBUUID UUIDWithString:@"6e400003-b5a3-f393-e0a9-e50e24dcca9e"];
    return [CBUUID UUIDWithString:UART_RX_CHARACTERISTIC_UUID];
}

- (CBUUID *)rwCharacteristicUUID
{
    // return [CBUUID UUIDWithString:@"6e400003-b5a3-f393-e0a9-e50e24dcca9e"];
    return [CBUUID UUIDWithString:UART_RW_CHARACTERISTIC_UUID];
}

- (CBUUID *)deviceInformationServiceUUID
{
    // return [CBUUID UUIDWithString:@"180A"];
    return [CBUUID UUIDWithString:UART_DEVICE_INFORMATION_UUID];
}

- (CBUUID *)hardwareRevisionStringUUID
{
    // return [CBUUID UUIDWithString:@"2A27"];
    return [CBUUID UUIDWithString:UART_HARDWARE_REVERSION_UUID];
}

#pragma mark - Initial

- (UARTPeripheral *)initWithPeripheral:(CBPeripheral *)peripheral uartDelegate:(id<UARTPeripheralDelegate>)uartDelegate RSSI:(int)RSSI
{
    if (self = [super init]) {
        self.peripheral = peripheral;
        _peripheral.delegate = self;
        self.uartDelegate = uartDelegate;
        self.RSSI = RSSI;
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    UARTPeripheral *other = (UARTPeripheral*) object;
    return _peripheral == other.peripheral;
}

#pragma mark - Connect & Disconnect

- (void)didConnectPeripheral
{
     [_peripheral discoverServices:@[self.class.uartServiceUUID, self.deviceInformationServiceUUID]];
}

- (void)didDisconnectPeripheral
{
    _peripheral = nil;
    _rxCharacteristic = nil;
    _txCharacteristic = nil;
    _rwCharacteristic = nil;
}

#pragma mark - Discovery

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"Error discovering services: %@", error]];
        return;
    }

    for (CBService *service in [peripheral services]) {
        if ([service.UUID isEqual:self.class.uartServiceUUID]) {
            self.uartService = service;
            [_peripheral discoverCharacteristics:@[self.txCharacteristicUUID, self.rxCharacteristicUUID] forService:_uartService];
        } else if ([service.UUID isEqual:self.deviceInformationServiceUUID]) {
            [_peripheral discoverCharacteristics:@[self.hardwareRevisionStringUUID] forService:service];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"Error discovering characteristics: %@", error]];
        return;
    }
    
    for (CBCharacteristic *characteristic in [service characteristics]) {
        if ([characteristic.UUID isEqual:self.rxCharacteristicUUID]) {
            self.rxCharacteristic = characteristic;
            [_peripheral setNotifyValue:YES forCharacteristic:_rxCharacteristic];
        } else if ([characteristic.UUID isEqual:self.txCharacteristicUUID]) {
            self.txCharacteristic = characteristic;
        } else if ([characteristic.UUID isEqual:self.rwCharacteristicUUID]) {
            self.rwCharacteristic = characteristic;
            [_peripheral setNotifyValue:YES forCharacteristic:_rwCharacteristic];
        } else if ([characteristic.UUID isEqual:self.hardwareRevisionStringUUID]) {
            [_peripheral readValueForCharacteristic:characteristic];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        [Utility showAlert:[NSString stringWithFormat:@"Error receiving notification for characteristic %@: %@", characteristic, error]];
        return;
    }
    
    if ([characteristic.UUID isEqual:self.hardwareRevisionStringUUID]) {
        NSString *hwRevision = @"";
        const uint8_t *bytes = characteristic.value.bytes;
        for (int i = 0; i < characteristic.value.length; i++) {
            hwRevision = [hwRevision stringByAppendingFormat:@"0X%02hhX, ", bytes[i]];
        }

        [_uartDelegate didReadHardwareRevisionString:[hwRevision substringToIndex:hwRevision.length - 2]];
    } else {
        [_uartDelegate didReceiveData:characteristic.value];
    }
}

#pragma mark - Write & Read

- (void)writeString:(NSString *)string
{
    NSData *data = [NSData dataWithBytes:string.UTF8String length:string.length];
    [self writeRawData:data];
}

- (void)writeRawData:(NSData *)data
{
    if ((_txCharacteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) != 0) {
        [_peripheral writeValue:data forCharacteristic:_txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    } else if ((_txCharacteristic.properties & CBCharacteristicPropertyWrite) != 0) {
        [_peripheral writeValue:data forCharacteristic:_txCharacteristic type:CBCharacteristicWriteWithResponse];
    } else {
        [Utility showAlert:[NSString stringWithFormat:@"Cannot write data for TX characteristic [%lu]", _txCharacteristic.properties]];
    }
}

@end
