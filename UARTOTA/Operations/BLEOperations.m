//
//  BLEOperations.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 07/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "BLEOperations.h"
#import "Utility.h"
#import "Constants.h"

@implementation BLEOperations

bool isDFUPacketCharacteristicFound, isDFUControlPointCharacteristic;

-(BLEOperations *) initWithDelegate:(id<BLEOperationsDelegate>) delegate
{
    if (self = [super init])
    {
        self.bleDelegate = delegate;        
    }
    return self;
}

-(void)setBluetoothCentralManager:(CBCentralManager *)manager
{
    self.centralManager = manager;
    self.centralManager.delegate = self;
}

-(void)connectDevice:(CBPeripheral *)peripheral
{
    self.bluetoothPeripheral = peripheral;
    self.bluetoothPeripheral.delegate = self;
    [self.centralManager connectPeripheral:peripheral options:nil];
}

-(void)searchDFURequiredCharacteristics:(CBService *)service
{
    isDFUControlPointCharacteristic = NO;
    isDFUPacketCharacteristicFound = NO;
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_CONTROL_CHARACTERISTIC_UUID]]) {
            isDFUControlPointCharacteristic = YES;
            self.dfuControlPointCharacteristic = characteristic;
        }
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_PACKET_CHARACTERISTIC_UUID]]) {
            isDFUPacketCharacteristicFound = YES;
            self.dfuPacketCharacteristic = characteristic;
        }
    }
}

#pragma mark - CentralManager delegates
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    // NSLog(@"centralManagerDidUpdateState");
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [self.bluetoothPeripheral discoverServices:nil];
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [self.bleDelegate onDeviceDisconnected:peripheral];
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [self.bleDelegate onDeviceDisconnected:peripheral];
}

#pragma mark - CBPeripheral delegates

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *service in peripheral.services) {
        if ([service.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_SERVICE_UUID]]) {
            [self.bluetoothPeripheral discoverCharacteristics:nil forService:service];
            return;
        }
    }
    NSString *errorMessage = [NSString stringWithFormat:@"Error on discovering service\n Message: Required DFU service not available on peripheral"];
    [self.centralManager cancelPeripheralConnection:peripheral];
    [self.bleDelegate onError:errorMessage];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    [self searchDFURequiredCharacteristics:service];
    if (isDFUControlPointCharacteristic && isDFUPacketCharacteristicFound) {        
        [self.bleDelegate onDeviceConnected:self.bluetoothPeripheral withPacketCharacteristic:self.dfuPacketCharacteristic andControlPointCharacteristic:self.dfuControlPointCharacteristic];
    }
    else {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on discovering characteristics\n Message: Required DFU characteristics are not available on peripheral"];
        [self.centralManager cancelPeripheralConnection:peripheral];
        [self.bleDelegate onError:errorMessage];
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on BLE Notification\n Message: %@",[error localizedDescription]];
        [self.bleDelegate onError:errorMessage];
    } else {
        [self.bleDelegate onReceivedNotification:characteristic.value];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Cannot write value to characteristic [%@] with error [%@]", characteristic.UUID, [error localizedDescription]);
    } else {
        NSLog(@"Did write value [%@] to characteristic [%@]", characteristic.value, characteristic.UUID);
    }
}


@end
