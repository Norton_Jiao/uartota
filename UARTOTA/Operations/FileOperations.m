//
//  FileOperations.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 03/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "FileOperations.h"
#import "IntelHex2BinConverter.h"
#import "Utility.h"
#import "Constants.h"

@implementation FileOperations

-(FileOperations *) initWithDelegate:(id<FileOperationsDelegate>) delegate blePeripheral:(CBPeripheral *)peripheral bleCharacteristic:(CBCharacteristic *)dfuPacketCharacteristic;
{
    self = [super init];
    if (self)
    {
        self.fileDelegate = delegate;
        self.bluetoothPeripheral = peripheral;
        self.dfuPacketCharacteristic = dfuPacketCharacteristic;
    }
    return self;
}

-(void)openFile:(NSURL *)fileURL
{
    NSData *fileData = [NSData dataWithContentsOfURL:fileURL];
    if (fileData.length > 0) {
        [self convertBinaryDataFromFileData:fileData];
        [self.fileDelegate onFileOpened:self.binFileSize];
    } else {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on openning file\n Message: file is empty or not exist"];
        [self.fileDelegate onError:errorMessage];
    }
}

- (void)convertBinaryDataFromFileData:(NSData *)fileData
{
    NSString *fileType = [[NSUserDefaults standardUserDefaults] objectForKey:DFU_FILE_TYPE];
    if ([fileType isEqualToString:@"BIN"]) {
        self.binFileData = fileData;
    } else {
        self.binFileData = [IntelHex2BinConverter convert:fileData];
    }

    self.numberOfPackets = ceil((double)self.binFileData.length / (double)PACKET_SIZE);
    self.bytesInLastPacket = (self.binFileData.length % PACKET_SIZE);
    if (self.bytesInLastPacket == 0) {
        self.bytesInLastPacket = PACKET_SIZE;
    }
    self.writingPacketNumber = 0;
    self.binFileSize = self.binFileData.length;
}

-(void)writeNextPacket
{
    int percentage = 0;
    for (int index = 0; index < PACKETS_NOTIFICATION_INTERVAL; index++) {
        if (self.writingPacketNumber > self.numberOfPackets - 2) {
            NSRange dataRange = NSMakeRange(self.writingPacketNumber * PACKET_SIZE, self.bytesInLastPacket);
            NSData *nextPacketData = [self.binFileData subdataWithRange:dataRange];
            [self.bluetoothPeripheral writeValue:nextPacketData forCharacteristic:self.dfuPacketCharacteristic type:CBCharacteristicWriteWithoutResponse];
            self.writingPacketNumber++;
            [self.fileDelegate onAllPacketsTranferred];            
            break;
        }
        NSRange dataRange = NSMakeRange(self.writingPacketNumber * PACKET_SIZE, PACKET_SIZE);
        NSData *nextPacketData = [self.binFileData subdataWithRange:dataRange];
        [self.bluetoothPeripheral writeValue:nextPacketData forCharacteristic:self.dfuPacketCharacteristic type:CBCharacteristicWriteWithoutResponse];
        percentage = (((double)(self.writingPacketNumber * 20) / (double)(self.binFileSize)) * 100);
        [self.fileDelegate onTransferPercentage:percentage];
        self.writingPacketNumber++;
        
    }

}

-(void)setBLEParameters:(CBPeripheral *)peripheral bleCharacteristic:(CBCharacteristic *)dfuPacketCharacteristic
{
    self.bluetoothPeripheral = peripheral;
    self.dfuPacketCharacteristic = dfuPacketCharacteristic;
}

@end
