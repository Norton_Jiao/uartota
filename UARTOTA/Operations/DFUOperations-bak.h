//
//  DFUOperations.h
//  nRFDeviceFirmwareUpdate
//
//  Created by Nordic Semiconductor on 18/06/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "DFUOperationsDetails.h"
#import "Utility.h"
#import "FileOperations.h"
#import "BLEOperations.h"


@class DFUOperations;

//define protocol for the delegate
@protocol DFUOperationsDelegate

//define protocol functions that can be used in any class using this delegate
- (void)onDeviceConnected:(CBPeripheral *)peripheral;
- (void)onDeviceDisconnected:(CBPeripheral *)peripheral;
- (void)onDFUStarted;
- (void)onDFUCancelled;
- (void)onSoftDeviceUploadStarted;
- (void)onSoftDeviceUploadCompleted;
- (void)onBootloaderUploadStarted;
- (void)onBootloaderUploadCompleted;
- (void)onTransferPercentage:(int)percentage;
- (void)onSuccessfulFileTranferred;
- (void)onError:(NSString *)errorMessage;

@end

@interface DFUOperations : NSObject

@property (assign, nonatomic) NSUInteger binFileSize;
@property (assign, nonatomic) NSUInteger uploadTimeInSeconds;

- (DFUOperations *)initWithDelegate:(id<DFUOperationsDelegate>) delegate;

//define public methods
- (void)setCentralManager:(CBCentralManager *)manager;
- (void)connectDevice:(CBPeripheral *)peripheral;
- (void)performDFUOnFile:(NSURL *)firmwareURL firmwareType:(DFUFirmwareTypes)firmwareType;
- (void)performDFUOnFiles:(NSURL *)softdeviceURL bootloaderURL:(NSURL *)bootloaderURL firmwareType:(DFUFirmwareTypes)firmwareType;
- (void)performOldDFUOnFile:(NSURL *)firmwareURL;

- (void)cancelDFU;

@end
