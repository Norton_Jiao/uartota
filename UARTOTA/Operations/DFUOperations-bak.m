//
//  DFUOperations.m
//  nRFDeviceFirmwareUpdate
//
//  Created by Nordic Semiconductor on 18/06/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "DFUOperations.h"
#import "Utility.h"
#import "IntelHex2BinConverter.h"
#import "DFUOperationsDetails.h"
#import "BLEOperations.h"

@interface DFUOperations() <BLEOperationsDelegate, FileOperationsDelegate>

@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) CBCharacteristic *packetCharacteristic;
@property (strong, nonatomic) CBCharacteristic *controlCharacteristic;

@property (strong, nonatomic) BLEOperations *bleOperations;
@property (strong, nonatomic) DFUOperationsDetails *dfuRequests;
@property (strong, nonatomic) FileOperations *fileRequests1;
@property (strong, nonatomic) FileOperations *fileRequests2;
@property (assign, nonatomic) DFUFirmwareTypes dfuFirmwareType;

@property (strong, nonatomic) NSURL *firmwareFile;
@property (assign, nonatomic) id<DFUOperationsDelegate> dfuDelegate;

@property struct DFUResponse dfuResponse;

@end

@implementation DFUOperations

bool isStartingSecondFile, isPerformedOldDFU;
NSDate *startTime, *finishTime;
double const delayInSeconds = 10.0;

- (DFUOperations *) initWithDelegate:(id<DFUOperationsDelegate>) delegate
{
    if (self = [super init])
    {
        self.dfuDelegate = delegate;
        self.dfuRequests = [[DFUOperationsDetails alloc] init];
        self.bleOperations = [[BLEOperations alloc] initWithDelegate:self];
        
    }
    return self;
}


- (void)setCentralManager:(CBCentralManager *)manager
{
    if (manager) {
        [_bleOperations setBluetoothCentralManager:manager];
    } else {
        NSLog(@"CBCentralManager is nil");
        NSString *errorMessage = [NSString stringWithFormat:@"Error on received CBCentralManager\n Message: Bluetooth central manager is nil"];
        [_dfuDelegate onError:errorMessage];
    }
}

- (void)connectDevice:(CBPeripheral *)peripheral
{
    if (peripheral) {
        NSLog(@"Norton======Connecting the Matched device.");
        [_bleOperations connectDevice:peripheral];
    } else {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on received CBPeripheral\n Message: Bluetooth peripheral is nil"];
        [_dfuDelegate onError:errorMessage];
    }
}

- (void)cancelDFU
{
    NSLog(@"cancelDFU");
    [_dfuRequests resetSystem];
    [_dfuDelegate onDFUCancelled];
}

- (void)performDFUOnFiles:(NSURL *)softdeviceURL bootloaderURL:(NSURL *)bootloaderURL firmwareType:(DFUFirmwareTypes)firmwareType
{
    NSLog(@"performDFUOnFiles:bootloaderURL:firmwareType:");
    isPerformedOldDFU = NO;
    [self initFirstFileOperations];
    [self initSecondFileOperations];
    [self initParameters];
    self.dfuFirmwareType = firmwareType;
    [_fileRequests1 openFile:softdeviceURL];
    [_fileRequests2 openFile:bootloaderURL];
    [_dfuRequests enableNotification];
    [_dfuRequests startDFU:firmwareType];
    [_dfuRequests writeFilesSizes:(uint32_t)_fileRequests1.binFileSize bootloaderSize:(uint32_t)_fileRequests2.binFileSize];
}

- (void)performDFUOnFile:(NSURL *)firmwareURL firmwareType:(DFUFirmwareTypes)firmwareType
{
    NSLog(@"performDFUOnFiles:firmwareType:======%hhu", firmwareType);
    isPerformedOldDFU = NO;
    self.firmwareFile = firmwareURL;
    [self initFirstFileOperations];
    isStartingSecondFile = NO;
    [self initParameters];
    self.dfuFirmwareType = firmwareType;
    [_fileRequests1 openFile:firmwareURL];
    [_dfuRequests enableNotification];
    [_dfuRequests startDFU:firmwareType];
    [_dfuRequests writeFileSize:(uint32_t)_fileRequests1.binFileSize];
}

- (void)performOldDFUOnFile:(NSURL *)firmwareURL
{
    isPerformedOldDFU = YES;
    if (firmwareURL && _dfuFirmwareType == APPLICATION) {
        [self initFirstFileOperations];
        [self initParameters];
        [_fileRequests1 openFile:firmwareURL];
        [_dfuRequests enableNotification];
        [_dfuRequests startOldDFU];
        [_dfuRequests writeFileSizeForOldDFU:(uint32_t)_fileRequests1.binFileSize];
    } else {
        NSString *errorMessage = [NSString stringWithFormat:@"Old DFU only supports Application upload"];
        [_dfuDelegate onError:errorMessage];
        [_dfuRequests resetSystem];
    }
    
}

-(void)initParameters
{
    startTime = [NSDate date];
    self.binFileSize = 0;
    isStartingSecondFile = NO;
}

-(void)initFirstFileOperations
{
    self.fileRequests1 = [[FileOperations alloc] initWithDelegate:self
                                                    blePeripheral:_peripheral
                                                bleCharacteristic:_packetCharacteristic];
}

-(void)initSecondFileOperations
{
    self.fileRequests2 = [[FileOperations alloc] initWithDelegate:self
                                                    blePeripheral:_peripheral
                                                bleCharacteristic:_packetCharacteristic];
}

-(void) startSendingFile
{
    if (_dfuFirmwareType == SOFTDEVICE || _dfuFirmwareType == SOFTDEVICE_AND_BOOTLOADER) {
        NSLog(@"waiting 10 seconds before sending file ...");
        //Delay of 10 seconds is required in order to update Softdevice in SDK 6.0
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [_dfuRequests enablePacketNotification];
            [_dfuRequests receiveFirmwareImage];
            [_fileRequests1 writeNextPacket];
            [_dfuDelegate onDFUStarted];
        });
    } else {
        [_dfuRequests enablePacketNotification];
        [_dfuRequests receiveFirmwareImage];
        [_fileRequests1 writeNextPacket];
        [_dfuDelegate onDFUStarted];
    }
    if (_dfuFirmwareType == SOFTDEVICE_AND_BOOTLOADER) {
        [_dfuDelegate onSoftDeviceUploadStarted];
    }
}

- (NSString *)responseErrorMessage:(DFUOperationStatus)errorCode
{
    switch (errorCode) {
        case OPERATION_FAILED_RESPONSE:
            return @"Operation Failed";
            break;
        case OPERATION_INVALID_RESPONSE:
            return @"Invalid Response";
            break;
        case OPERATION_NOT_SUPPORTED_RESPONSE:
            return @"Operation Not Supported";
            break;
        case DATA_SIZE_EXCEEDS_LIMIT_RESPONSE:
            return @"Data Size Exceeds";
            break;
        case CRC_ERROR_RESPONSE:
            return @"CRC Error";
            break;
        default:
            return @"unknown Error";
            break;
    }
}

-(void)processRequestedCode
{
    NSLog(@"processsRequestedCode");
    switch (_dfuResponse.requestedCode) {
        case START_DFU_REQUEST:
            NSLog(@"Norton============Requested code is StartDFU now processing response status");
            [self processStartDFUResponseStatus];
            break;
        case RECEIVE_FIRMWARE_IMAGE_REQUEST:
            NSLog(@"Requested code is Receive Firmware Image now processing response status");
            [self processReceiveFirmwareResponseStatus];
            break;
        case VALIDATE_FIRMWARE_REQUEST:
            NSLog(@"Requested code is Validate Firmware now processing response status");
            [self processValidateFirmwareResponseStatus];
            break;
            
        default:
            NSLog(@"invalid Requested code in DFU Response %d", _dfuResponse.requestedCode);
            break;
    }
}

-(void)processStartDFUResponseStatus
{
    NSLog(@"processStartDFUResponseStatus");
    NSString *errorMessage = [NSString stringWithFormat:@"Error on StartDFU\n Message: %@",[self responseErrorMessage:_dfuResponse.responseStatus]];
    switch (_dfuResponse.responseStatus) {
        case OPERATION_SUCCESSFUL_RESPONSE:
            NSLog(@"successfully received startDFU notification");
            [self startSendingFile];
            break;
        case OPERATION_NOT_SUPPORTED_RESPONSE:
            if (!isPerformedOldDFU) {
                NSLog(@"device has old DFU. switching to old DFU ...");
                [self performOldDFUOnFile:_firmwareFile];
            }
            else {
                NSLog(@"Operation not supported");
                NSLog(@"Firmware Image failed, Error Status: %@",[self responseErrorMessage:_dfuResponse.responseStatus]);
                NSString *errorMessage = [NSString stringWithFormat:@"Error on StartDFU\n Message: %@",[self responseErrorMessage:_dfuResponse.responseStatus]];
                [_dfuDelegate onError:errorMessage];
                [_dfuRequests resetSystem];
            }
            break;
            
        default:
            NSLog(@"StartDFU failed, Error Status: %@",[self responseErrorMessage:_dfuResponse.responseStatus]);
            [_dfuDelegate onError:errorMessage];
            [_dfuRequests resetSystem];
            break;
    }
}

-(void)processReceiveFirmwareResponseStatus
{
    NSLog(@"processReceiveFirmwareResponseStatus=======_dfuResponse.responseStatus");
    if (_dfuResponse.responseStatus == OPERATION_SUCCESSFUL_RESPONSE) {
        NSLog(@"successfully received notification for whole File transfer");
        [_dfuRequests validateFirmware];
    } else {
        NSLog(@"Firmware Image failed, Error Status: %@", [self responseErrorMessage:_dfuResponse.responseStatus]);
        NSString *errorMessage = [NSString stringWithFormat:@"Error on Receive Firmware Image\n Message: %@",[self responseErrorMessage:_dfuResponse.responseStatus]];
        [_dfuDelegate onError:errorMessage];
        [_dfuRequests resetSystem];
    }
}

-(void)processValidateFirmwareResponseStatus
{
    NSLog(@"processValidateFirmwareResponseStatus");
    if (_dfuResponse.responseStatus == OPERATION_SUCCESSFUL_RESPONSE) {
        NSLog(@"succesfully received notification for ValidateFirmware");
        [_dfuRequests activateAndReset];
        [self calculateDFUTime];
        [_dfuDelegate onSuccessfulFileTranferred];
    } else {
        NSLog(@"Firmware validate failed, Error Status: %@",[self responseErrorMessage:_dfuResponse.responseStatus]);
        NSString *errorMessage = [NSString stringWithFormat:@"Error on Validate Firmware Request\n Message: %@",[self responseErrorMessage:_dfuResponse.responseStatus]];
        [_dfuDelegate onError:errorMessage];
        [_dfuRequests resetSystem];
    }
}

-(void)processPacketNotification
{
    NSLog(@"received Packet Received Notification");
    if (isStartingSecondFile) {
        if (_fileRequests2.writingPacketNumber < _fileRequests2.numberOfPackets) {
            [_fileRequests2 writeNextPacket];
        }
    } else {
        if (_fileRequests1.writingPacketNumber < _fileRequests1.numberOfPackets) {
            [_fileRequests1 writeNextPacket];
        }
    }
}

-(void)processDFUResponse:(uint8_t *)data
{
    [self setDFUResponseStruct:data];
    if (_dfuResponse.responseCode == RESPONSE_CODE) {
        NSLog(@"Norton===============RESPONSE_CODE====0X10");
        [self processRequestedCode];
    } else if(_dfuResponse.responseCode == PACKET_RECEIPT_NOTIFICATION_RESPONSE) {
        [self processPacketNotification];
    }
}

-(void)setDFUResponseStruct:(uint8_t *)data
{
    _dfuResponse.responseCode = data[0];
    _dfuResponse.requestedCode = data[1];
    _dfuResponse.responseStatus = data[2];
}

-(void)setDFUOperationsDetails
{
    [self.dfuRequests setPeripheralAndOtherParameters:_peripheral
                           controlPointCharacteristic:_controlCharacteristic
                                 packetCharacteristic:_packetCharacteristic];
}

-(void)calculateDFUTime
{
    finishTime = [NSDate date];
    self.uploadTimeInSeconds = [finishTime timeIntervalSinceDate:startTime];
    NSLog(@"upload time in sec: %lu", (unsigned long)_uploadTimeInSeconds);
}

#pragma mark - BLEOperations delegates

-(void)onDeviceConnected:(CBPeripheral *)peripheral withPacketCharacteristic:(CBCharacteristic *)packetCharacteristic andControlPointCharacteristic:(CBCharacteristic *)controlCharacteristic
{
    self.peripheral = peripheral;
    self.packetCharacteristic = packetCharacteristic;
    self.controlCharacteristic = controlCharacteristic;
    [self setDFUOperationsDetails];
    [_dfuDelegate onDeviceConnected:peripheral];
}

- (void)onDeviceDisconnected:(CBPeripheral *)peripheral
{
    [_dfuDelegate onDeviceDisconnected:peripheral];
}

-(void)onReceivedNotification:(NSData *)data
{
    NSLog(@"-------Received data === %@", data);
    [self processDFUResponse:(uint8_t *)[data bytes]];
}

#pragma mark - FileOperations delegates

-(void)onTransferPercentage:(int)percentage
{
    NSLog(@"DFUOperations: onTransferPercentage %d",percentage);
    [_dfuDelegate onTransferPercentage:percentage];
}

-(void)onAllPacketsTranferred
{
    NSLog(@"DFUOperations: onAllPacketsTransfered");
    if (isStartingSecondFile) {
        [_dfuDelegate onBootloaderUploadCompleted];
    } else if (_dfuFirmwareType == SOFTDEVICE_AND_BOOTLOADER) {
        isStartingSecondFile = YES;
        NSLog(@"Firmware type is Softdevice plus Bootloader. now upload bootloader ...");
        [_dfuDelegate onSoftDeviceUploadCompleted];
        [_dfuDelegate onBootloaderUploadStarted];
        [_fileRequests2 writeNextPacket];
    }
}

-(void)onFileOpened:(NSUInteger)fileSizeOfBin
{
    NSLog(@"onFileOpened file size: %lu",(unsigned long)fileSizeOfBin);
    _binFileSize += fileSizeOfBin;
}

-(void)onError:(NSString *)errorMessage
{
    NSLog(@"DFUOperations: onError");
    [_dfuDelegate onError:errorMessage];
}

@end

