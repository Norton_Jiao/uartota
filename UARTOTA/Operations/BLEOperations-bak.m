//
//  BLEOperations.m
//  nRF Toolbox
//
//  Created by Nordic Semiconductor on 07/07/14.
//  Copyright (c) 2014 Nordic Semiconductor. All rights reserved.
//

#import "BLEOperations.h"
#import "Utility.h"
#import "Constants.h"

@interface BLEOperations ()

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *uartPeripheral;
@property (strong, nonatomic) CBCharacteristic *packetCharacteristic;
@property (assign, nonatomic) BOOL packetCharacteristicFound;
@property (strong, nonatomic) CBCharacteristic *controlCharacteristic;
@property (assign, nonatomic) BOOL controlCharacteristicFound;

@end

@implementation BLEOperations

- (BLEOperations *)initWithDelegate:(id<BLEOperationsDelegate>) delegate
{
    if (self = [super init]) {
        self.bleDelegate = delegate;        
    }
    return self;
}

- (void)setBluetoothCentralManager:(CBCentralManager *)manager
{
    self.centralManager = manager;
    _centralManager.delegate = self;
}

- (void)connectDevice:(CBPeripheral *)peripheral
{
    self.uartPeripheral = peripheral;
    _uartPeripheral.delegate = self;
    [_centralManager connectPeripheral:peripheral options:nil];
}

- (void)searchDFURequiredCharacteristics:(CBService *)service
{
    self.packetCharacteristicFound = NO;
    self.controlCharacteristicFound = NO;
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_PACKET_CHARACTERISTIC_UUID]]) {
            self.packetCharacteristicFound = YES;
            self.packetCharacteristic = characteristic;
            NSLog(@"Norton======Packet characteristic found.");
        }
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_CONTROL_CHARACTERISTIC_UUID]]) {
            self.controlCharacteristicFound = YES;
            self.controlCharacteristic = characteristic;
            NSLog(@"Norton======Control characteristic found.");
        }
    }
}

#pragma mark - CentralManager delegates

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"centralManagerDidUpdateState");
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Norton======Connected peripheral [%@] with identifier [%@]", peripheral.name, [peripheral.identifier UUIDString]);
    [_uartPeripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Peripheral disconnected because of [%@]", error);
    [self.bleDelegate onDeviceDisconnected:peripheral];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [self.bleDelegate onDeviceDisconnected:peripheral];
}

#pragma mark - CBPeripheral delegates

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *service in peripheral.services) {
        if ([service.UUID isEqual:[CBUUID UUIDWithString:BOOTLOADER_SERVICE_UUID]]) {
            NSLog(@"DFU Service is found");
            [_uartPeripheral discoverCharacteristics:nil forService:service];
            return;
        }
    }
    NSString *errorMessage = [NSString stringWithFormat:@"Error on discovering service\n Message: Required DFU service not available on peripheral"];
    [_centralManager cancelPeripheralConnection:peripheral];
    [self.bleDelegate onError:errorMessage];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    [self searchDFURequiredCharacteristics:service];
    if (_controlCharacteristicFound && _packetCharacteristicFound) {
        [self.bleDelegate onDeviceConnected:_uartPeripheral withPacketCharacteristic:_packetCharacteristic andControlPointCharacteristic:_controlCharacteristic];
    } else {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on discovering characteristics\n Message: Required DFU characteristics are not available on peripheral"];
        [_centralManager cancelPeripheralConnection:peripheral];
        [self.bleDelegate onError:errorMessage];
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSString *errorMessage = [NSString stringWithFormat:@"Error on BLE Notification\n Message: %@",[error localizedDescription]];
        NSLog(@"Error in Notification state: %@",[error localizedDescription]);
        [self.bleDelegate onError:errorMessage];
    } else {
        NSLog(@"-----------received notification %@",characteristic.value);
        [self.bleDelegate onReceivedNotification:characteristic.value];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"error in writing characteristic %@ and error %@", characteristic.UUID, [error localizedDescription]);
        /*NSString *errorMessage = [NSString stringWithFormat:@"Error on Writing Characteristic %@\n Message: %@",characteristic.UUID,[error localizedDescription]];
         [dfuDelegate onError:errorMessage];*/
    } else {
        NSLog(@"didWriteValueForCharacteristic %@ and value %@",characteristic.UUID,characteristic.value);
    }
}

/*
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    if (error) {
        NSLog(@"error in writing descriptor %@ and error %@", descriptor.UUID, [error localizedDescription]);
        // NSString *errorMessage = [NSString stringWithFormat:@"Error on Writing Characteristic %@\n Message: %@",characteristic.UUID,[error localizedDescription]];
        // [dfuDelegate onError:errorMessage];
    } else {
        NSLog(@"didWriteValueForDescriptor %@ and value %@", descriptor.UUID, descriptor.value);
    }
}
*/

@end
